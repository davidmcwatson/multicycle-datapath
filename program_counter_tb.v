module pc_tb();
	
	reg clk;
	reg rst;
	reg inc;
	reg write_en;

	reg [19:0] write_data;
	wire [19:0] ctr;

	pc dut(clk, rst, inc, write_en, write_data, ctr);

	initial begin
		clk = 1;
		forever #10 clk = ~clk;
	end

	initial begin
		$monitor ($time, ,"%h  inc=%b, rst=%b, write=%b, write_data=%h",ctr, inc, rst, write_en, write_data);
		write_data = 0;
		write_en = 0;
		inc = 0;
		rst = 1;
		#50
		
		rst = 0;
		inc = 1;
		#200;
		
		inc = 0;
		rst = 1;
		#40

		rst = 0;
		write_data = 46;
		write_en = 1;
		#20

		inc = 1;
		write_en = 0;
		#40		

		$finish;
	end
endmodule
