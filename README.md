# Multi Cycle Datapath #

A small collection of verilog modules that would be found in a multicyle processor. Completed for Assignment 2 of COMP311 at the University of Waikato.
Modules implemented:
* Ripple Carry Addition module built from full and half adder modules.
* Program counter
* Register file with 16 32 bit registers. Two registers can be read in one cycle or one can be set.
* ALU supporting 5 functions - Add, Sub, AND, OR, XOR

### How do I get set up? ###

The project contains 4 verilog source files which contain the modules and 4 test bench files.
Import the files into a vivado project (or other verilog software).

### Author ###

* David Watson - hello@davidwatson.nz