module reg_file (
        input clk,
        input rst,

        input [3:0] addr1,
        input [3:0] addr2,
        input write_en,
        input [31:0] write_data,
        output reg [31:0] read_data1,
        output reg [31:0] read_data2 );
        
        reg [31:0] mem [0:15];
        integer j;
        
        initial begin
            for(j = 0; j < 16; j = j+1)
                mem[j] = 0;
        end
                
        always @(posedge clk) begin
            if (rst) begin
               for(j = 0; j < 16; j = j+1)
                   mem[j] = 0;
            end
            
            if (write_en) begin
                mem[addr1] <= write_data;
            end
            
            read_data1 <= mem[addr1];
            read_data2 <= mem[addr2];
        end

endmodule