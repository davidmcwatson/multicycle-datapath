module alu(
    input clk,
    input [31:0] operand1,
    input [31:0] operand2,
    input [3:0] func,
    output reg zero,
    output reg [31:0] result
    );
    
    // Same as WRAMP func code.
    // add = 0x0
    // sub = 0x2
    // and = 0xb
    // or  = 0xd
    // xor = 0xf
    
    always @(posedge clk) begin
        zero = 0;
        result = 0;
        case(func)
            0: assign {zero, result} = {1'b0, operand1} + {1'b0, operand2};
            2: assign result = operand1 - operand2;
            11: assign result = operand1 & operand2;
            13: assign result = operand1 | operand2;
            15: assign result = operand1 ^ operand2;
        endcase
    end
    
endmodule
