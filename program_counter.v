module pc(
	    input clk,
	    input rst,
	    input inc,
	    input write_en,

	    input [19:0] write_data,
	    output reg [19:0] ctr
    );

    always @(posedge clk) begin
        if(inc) begin
            ctr <= ctr + 1;
        end
        if(write_en) begin
            ctr <= write_data;
        end 
        if(rst) begin
            ctr <= 0;
        end 
    end 
endmodule
