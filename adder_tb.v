module full_adder_tb();
    reg in_x;
    reg in_y;
    reg carry_in;
    wire sum;
    wire carry_out;
    
    full_adder dut( in_x, in_y, carry_in, sum, carry_out );
    
    initial
        begin
          $monitor ($time, ,"x=%b,y=%b,in=%b,sum=%b,carry_out=%b",in_x,in_y,carry_in,sum,carry_out);
          
          // Test 0 + 0 + 0
          in_x = 0;
          in_y = 0;
          carry_in = 0;
          #50
          if( sum | carry_out ) begin
            $display("Failed!");
          end
          
          // Test 1 + 0 + 0
          in_x = 1;
          in_y = 0;
          carry_in = 0;
          #50
          if(~sum | carry_out) begin
            $display("Failed!");
          end
          
          // Test 1 + 0 + 1
          in_x = 1;
          in_y = 0;
          carry_in = 1;
          #50
          if(sum | ~carry_out) begin
             $display("Failed!");
          end
          
          // Test 1 + 1 + 0
          in_x = 1;
          in_y = 1;
          carry_in = 0;
          #50
          if(sum | ~carry_out) begin
            $display("Failed!");
          end
          
          // test 1 + 1 + 1
          carry_in = 1;
          #50
          if(~sum | ~carry_out) begin
            $display("Failed!");
          end
        end
    
endmodule

module ripple_carry_adder_tb ();
 
  parameter WIDTH = 20;
 
  reg [WIDTH-1:0] r_ADD_1 = 0;
  reg [WIDTH-1:0] r_ADD_2 = 0;
  wire [WIDTH-1:0]  w_RESULT;
  wire overflow;
 
  ripple_carry_adder #(.WIDTH(WIDTH)) dut
    (
     .in_x(r_ADD_1),
     .in_y(r_ADD_2),
     .out(w_RESULT),
     .overflow(overflow)
     );
 
  initial
    begin
      $display("\t\t\t\tTime\t\tX\t\tY\t\tSUM\t\tOVERFLOW");
      $monitor ($time, ,"\t\t%h,\t%h,\t%h,\t%b",r_ADD_1,r_ADD_2,w_RESULT,overflow);
      #100;
      r_ADD_1 = 0;
      r_ADD_2 = 1;
      #100;
      if(w_RESULT != 1) begin
        $display("FAILED");
      end
      r_ADD_1 = 20;
      r_ADD_2 = 36;
      #100;
      if(w_RESULT != 56) begin
        $display("FAILED");
      end
      r_ADD_1 = 1048575;
      r_ADD_2 = 1;
      #100;
      if(w_RESULT != 0 || overflow != 1) begin
        $display("FAILED");
      end
      r_ADD_1 = 1046575;
      r_ADD_2 = 10241;
      #100;
      if(w_RESULT != 8240 || overflow != 1) begin
        $display("FAILED");
      end
    end
 
endmodule // ripple_carry_adder_tb