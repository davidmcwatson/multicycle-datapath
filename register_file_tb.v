module reg_file_tb();

    reg clk;
    reg rst;
    
    reg [3:0] addr1;
    reg [3:0] addr2;
    reg write_en;
    reg [31:0] write_data;
    wire [31:0] read_data1;
    wire [31:0] read_data2;

    reg_file dut( clk, rst, addr1, addr2, write_en, write_data, read_data1, read_data2 );
    
    initial begin
            clk = 1;
            forever #10 clk = ~clk;
    end

    initial begin
        $monitor($time, ,"in1=%h, in2=%h, write=%b, data: %h  out1: %h  out2: %h  ", addr1, addr2, write_en, write_data, read_data1, read_data2 );
        rst = 1;
        write_en = 0;
        write_data = 0;
        addr1 = 1;
        addr2 = 2;
        #40;
        
        rst = 0;
        addr1 = 2;
        addr2 = 5;
        
        write_data = 4276993775;
        addr1 = 8;
        write_en = 1;
        #20;
        
        write_data = 4207849484;
        addr1 = 7;
        write_en = 1;
        #20;
        
        write_en = 0;
        write_data = 0;
        
        addr1 = 7;
        addr2 = 8;
        #50;
        if(read_data1 != 4207849484 || read_data2 != 4276993775) begin
            $display("FAILED - read back not the same as written");
        end
        $finish;
    end
    
endmodule
