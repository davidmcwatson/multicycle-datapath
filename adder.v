module half_adder(
    input in_x,
    input in_y,
    output out_sum,
    output carry_out
);

    assign out_sum = in_x ^ in_y;
    assign carry_out = in_x & in_y;
    
endmodule 

module full_adder(
    input in_x,
    input in_y,
    input carry_in,
    output out_sum,
    output carry_out
);

    wire temp_carry1;
    wire temp_carry2;
    wire temp_value;
    
    half_adder ha1( .in_x(in_x), .in_y(in_y), .out_sum(temp_value), .carry_out(temp_carry1) );
    half_adder ha2( .in_x(temp_value), .in_y(carry_in), .out_sum(out_sum), .carry_out(temp_carry2) );
    
    assign carry_out = temp_carry1 | temp_carry2;

endmodule

module ripple_carry_adder
    #(parameter WIDTH = 2)
    (
    input [WIDTH-1:0] in_x,
    input [WIDTH-1:0] in_y,
    output [WIDTH-1:0] out,
    output overflow
);

    wire [WIDTH:0] w_carry;
    // No carry input on first full adder  
    assign w_carry[0] = 1'b0; 
    
    generate
        for(genvar i=0; i < WIDTH; i=i+1)
        begin
            full_adder fa
                (
                    .in_x(in_x[i]),
                    .in_y(in_y[i]),
                    .carry_in(w_carry[i]),
                    .out_sum(out[i]),
                    .carry_out(w_carry[i+1])
                );
        end
    endgenerate

    assign overflow = w_carry[WIDTH];
    
endmodule