module alu_tb();

    reg clk;
    reg [31:0] operand1;
    reg [31:0] operand2;
    reg [3:0] func;
    wire zero;
    wire [31:0] result;
    
    alu dut(clk, operand1, operand2, func, zero, result);
    
    initial begin
        clk = 1;
        forever #10 clk = ~clk;
    end
    
    // Same as WRAMP func code.
        // add = 0x0
        // sub = 0x2
        // and = 0xb
        // or  = 0xd
        // xor = 0xf
        
    initial begin
        $monitor ($time, ,"x=%h,y=%h,func=%h,result=%h,overflow=%b",operand1,operand2,func,result,zero);
        
        // Set test values
        operand1 = 56;
        operand2 = 8;
        
        // Test add
        func = 0;
        #80;
        if(result != 64) begin
            $display("FAILED - ALU addition");
        end
        // Test sub
        func = 2;
        #80;
        if(result != 48) begin
            $display("FAILED - ALU subtraction");
        end
        // Test and
        func = 11;
        #80;
        if(result != 8) begin
            $display("FAILED - ALU bitwise AND");
        end
        // Test or
        func = 13;
        #80;
        if(result != 56) begin
            $display("FAILED - ALU bitwise OR");
        end
        // Test Xor
        func = 15;
        #80;
        if(result != 48) begin
            $display("FAILED - ALU bitwise XOR");
        end
        
        // Test addition overflow
        func = 0;
        operand1 = 4294967295;
        operand2 = 1;
        #80;
        // we should now see zero set
        if(zero != 1) begin
            $display("FAILED - ALU Overflow");
        end
    end 


endmodule
